const fs = require('fs');

// Read and parse package.json
/**
 * Reads the content of the 'package.json' file, increments the last version number,
 * updates the version in the file, and logs the updated version.
 */
const packageJson = JSON.parse(fs.readFileSync('package.json', 'utf8'));
const currentVersion = packageJson.version.split('.');
const patchVersion = parseInt(currentVersion[2]);
const newPatchVersion = patchVersion + 1;
packageJson.version = `${currentVersion[0]}.${currentVersion[1]}.${newPatchVersion}`;
fs.writeFileSync('package.json', JSON.stringify(packageJson, null, 2));
console.log(`Updated package version to: ${packageJson.version}`);