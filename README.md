# mime-validator


## Description
Validate file type against official MIME standard types

## Build
```javascript
npm run build
```

## Tests
Test are using jest.
To run tests use:
```bash
npm run test
```

## Usage
Validate method accepts string or array as arguments.
Returns boolean or throws exception if invalid argument.

```javascript
import { validate, mimeType } from 'mime-validator';

type = 'application/xml';

validate(type, 'xml'); // => true
validate(type, ['application/xml', 'application/rss+xml', 'application/pdf']) // => false
validate(type, [mimeType.PNG, mimeType.GIF, mimeType.JPEG]) // => false
```

## Contributing

We welcome and appreciate contributions from the community to help improve and enhance this project. Please follow these steps to contribute:


1. Fork the project to your GitLab account by clicking the "Fork" button at the top right of the project's GitLab page. This creates a copy of the project in your account.


2. Clone the forked repository to your local machine using Git. Replace `<your-username>` with your GitLab username.

```bash
git clone https://gitlab.com/<your-username>/mime-validator.git
```

3. Create a new branch for your feature or bug fix. Use a clear and descriptive branch name that reflects the purpose of your changes.
```bash
git checkout -b feature-name
```

4. Make changes

5. Add and commit your changes with clear and descriptive commit messages.
```bash
git add .
git commit -m "Add feature XYZ" or "Fix issue #123"
```
6. Push your branch to your GitLab fork:
```bash
git push origin feature-name
```
7. Create a Pull Request
    - Go to the GitLab page of your forked repository.
    - Click on the "New Pull Request" button.
    - Select the branch you just pushed (e.g., feature-name) as the source branch.
    - Provide a clear title and description for your pull request. Reference any related issues, if applicable.
    - Click the "Create Pull Request" button.




## License
MIT © [Branko Gabelica](https://gitlab.com/Gabelica)
