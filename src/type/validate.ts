import { MimeType } from '../index';

const fetchArray = (arg: string | string[]): string | string[] => {
    if (typeof arg !== 'string') {
        return arg;
    }
    if (typeof MimeType[arg] === 'function') {
        // Execute the function and return its result as an array
        const result: string[] = MimeType[arg]();
        if (Array.isArray(result)) {
            return result;
        }
    }
    // If the argument is not a method, return it
    return arg;
}


export const validate = (fileType: string, condition: string | string[]): boolean => {
    const types = fetchArray(condition);
    return types.includes(fileType);
}
