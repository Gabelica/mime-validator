/**
 * @class mimeTypes
 * Utility class that provides constants for various MIME types used in web development.
 * Categorizes MIME types into different groups such as text, image, audio, video, application, archive, font, XML, and binary.
 */
export class MimeType {
    static PLAIN        = "text/plain";
    static HTML         = "text/html";
    static CSS          = "text/css";
    static JAVASCRIPT   = "text/javascript";

    static JPEG     = "image/jpeg";
    static PNG      = "image/png";
    static GIF      = "image/gif";
    static SVG_XML  = "image/svg+xml";
    static BMP      = "image/bmp";
    static ICO      = "image/x-icon";
    static JLS      = "image/jls";
    static JP2      = "image/jp2";
    static JPF      = "image/jpx";
    static JPH      = "image/jph";
    static TIFF     = "image/tiff";
    static WEBP     = "image/webp";

    static AUDIO_MPEG   = "audio/mpeg";
    static AUDIO_OGG    = "audio/ogg";
    static AUDIO_MP3    = "audio/mp3";
    static AUDIO_WAV    = "audio/wav";
    static AUDIO_FLAC   = "audio/flac";
    static AUDIO_AAC    = "audio/aac";
    static AUDIO_FLV    = "audio/x-flv";
    static AUDIO_WMA    = "audio/x-ms-wma";

    static VIDEO_MP4        = "video/mp4";
    static VIDEO_WEBM       = "video/webm";
    static VIDEO_OGG        = "video/ogg";
    static VIDEO_AVI        = "video/x-msvideo";
    static VIDEO_QUICKTIME  = "video/quicktime";
    static VIDEO_FLV        = "video/x-flv";

    static POWERPOINT       = "application/vnd.ms-powerpoint";
    static PDF              = "application/pdf";
    static MSWORD           = "application/msword";
    static MS_EXCEL         = "application/vnd.ms-excel";
    static OPENXML_EXCEL    = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    static JSON             = "application/json";

    static ZIP      = "application/zip";
    static GZIP     = "application/x-gzip";
    static TAR      = "application/x-tar";
    static BZIP2    = "application/x-bzip2";
    static RAR      = "application/x-rar-compressed";
    static SEVENZIP = "application/x-7z-compressed";
    static LHA      = "application/x-lzh-compressed";

    static WOFF     = "font/woff";
    static WOFF2    = "font/woff2";
    static TTF      = "font/ttf";
    static OTF      = "font/otf";
    static EOT      = "application/vnd.ms-fontobject";
    static SFNT     = "application/font-sfnt";
    static SVG      = "image/svg+xml";

    static XML          = "application/xml";
    static RSS_XML      = "application/rss+xml";
    static ATOM_XML     = "application/atom+xml";
    static TEXT_XML     = "text/xml";
    static XHTML_XML    = "application/xhtml+xml";
    static SOAP_XML     = "application/soap+xml";
    static XOP_XML      = "application/xop+xml";

    static BINARY       = "application/octet-stream";
    static BSON         = "application/bson";
    static UBJSON       = "application/ubjson";
    static CBOR         = "application/cbor";
    static MESSAGE_PACK = "application/msgpack";
    static WASM         = "application/wasm";

    static CSV              = "text/csv";
    static APPLICATION_CSV  = "application/csv";
    static COMMA_SEPARATED  = "text/comma-separated-values";


    

    /**
     * Returns an array of MIME types that are used for text-based content.
     * 
     * @returns {string[]} An array of MIME types for text-based content, including plain text, HTML, CSS, and JavaScript.
     */
    static text(): string[] {
        return [
            this.PLAIN,
            this.HTML,
            this.CSS,
            this.JAVASCRIPT,
        ];
    }

    /**
     * Returns an array of MIME types that are used for files with Comma-Separated Values in content.
     * 
     * @returns {string[]} An array of MIME types for CSV files.
     */
    static csv(): string[] {
        return [
            this.CSV,
            this.APPLICATION_CSV,
            this.COMMA_SEPARATED,
        ];
    }

    static image(): string[] {
        /**
         * Returns an array of MIME types that are used for image-based content.
         * 
         * @returns {string[]} An array of MIME types for image-based content.
         */
        return [
            this.JPEG,
            this.PNG,
            this.GIF,
            this.SVG_XML,
            this.BMP,
            this.ICO,
            this.JLS,
            this.JP2,
            this.JPF,
            this.JPH,
            this.TIFF,
            this.WEBP
        ];
    }

    static audio(): string[] {
        /**
         * Returns an array of MIME types that are used for audio-based content.
         * 
         * @returns {string[]} An array of audio MIME types.
         */
        return [
            this.AUDIO_MPEG,
            this.AUDIO_OGG,
            this.AUDIO_MP3,
            this.AUDIO_WAV,
            this.AUDIO_FLAC,
            this.AUDIO_AAC,
            this.AUDIO_FLV,
            this.AUDIO_WMA
        ];
    }

    static video(): string[] {
        /**
         * Returns an array of MIME types that are used for video-based content.
         * 
         * @returns {string[]} An array of video MIME types, including `video/mp4`, `video/webm`, `video/ogg`, `video/x-msvideo`, `video/quicktime`, and `video/x-flv`.
         */
        return [
            this.VIDEO_MP4,
            this.VIDEO_WEBM,
            this.VIDEO_OGG,
            this.VIDEO_AVI,
            this.VIDEO_QUICKTIME,
            this.VIDEO_FLV
        ];
    }

    static application(): string[] {
        /**
         * Returns an array of MIME types that are commonly used for application-based content.
         * 
         * @returns {string[]} An array of MIME types for application-based content.
         */
        return [
            this.PDF,
            this.MSWORD,
            this.POWERPOINT,
            this.MS_EXCEL,
            this.OPENXML_EXCEL,
            this.JSON,
        ];
    }

    static archive(): string[] {
        /**
         * Returns an array of MIME types that are commonly used for archive-based content.
         * 
         * @returns {string[]} An array of MIME types for archive-based content.
         */
        return [
            this.ZIP,
            this.GZIP,
            this.TAR,
            this.BZIP2,
            this.RAR,
            this.SEVENZIP,
            this.LHA
        ];
    }


    static font(): string[] {
        /**
         * Returns an array of MIME types that are used for font-based content.
         * 
         * @returns {string[]} An array of MIME types for font-based content.
         */
        return [
            this.WOFF,
            this.WOFF2,
            this.SFNT,
            this.TTF,
            this.OTF,
            this.EOT,
            this.SVG
        ];
    }

    static xml(): string[] {
        /**
         * Returns an array of MIME types that are used for XML-based content.
         * 
         * @returns {string[]} An array of MIME types for XML-based content.
         */
        return [
            this.XML,
            this.RSS_XML,
            this.ATOM_XML,
            this.TEXT_XML,
            this.XHTML_XML,
            this.SOAP_XML,
            this.XOP_XML,
        ];
    }

    static binary(): string[] {
        /**
         * Returns an array of MIME types that are commonly used for binary-based content.
         * 
         * @returns {string[]} An array of MIME types for binary-based content.
         */
        return [
            this.BINARY,
            this.BSON,
            this.UBJSON,
            this.CBOR,
            this.MESSAGE_PACK,
            this.WASM
        ];
    }
}